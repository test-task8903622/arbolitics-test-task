### Task 2

This task seems pretty straightforward to me, all the specifics are described in the notebook. I once again used Python 3.9 and Pandas to work with data, and then trained a boosting and a linear regression model to predict the target. I also used Optuna to tune hyperparameters. The final MSE metric I achieved on the test set is 0.0098.

No other tools, other than Python and the relevant modules are needed to reproduce the experiments.