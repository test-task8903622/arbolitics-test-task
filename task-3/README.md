### Task 3

Task 3 is very similar to 2, with all the used techniques being basically the same, apart from the specific models used to get the predictions. The specifics of differences between tasks are explained in the notebook. The final accuracy metric on the test set is 0.985.

No other tools, other than Python and the relevant modules are needed to reproduce the experiments.