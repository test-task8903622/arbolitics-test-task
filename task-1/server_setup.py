import pandas as pd
from sqlalchemy import create_engine, select, text
from flask import Flask, request, jsonify
from credentials import user, password

def load_and_standardize(path):
    # Reading the dataframe
    df = pd.read_csv(path)

    # Extracting the necessary data from embedded lists
    df['data'] = df['data'].apply(lambda x: eval(x))
    df = df.explode('data').reset_index(drop=True)
    df['dt'] = df['data'].map(lambda x: x['dt'])
    df['temperature'] = df['data'].map(lambda x: x['main']['temp_min'])

    # Selecting relevant columns
    df = df[['_id', 'date', 'zone_id', 'dt', 'temperature']]
    return df

def create_db_engine(user, password):
    # Connects to the postgre database, returns the connection engine
    return create_engine(f'postgresql://{user}:{password}@localhost:5432/weather_db')

def load_to_postgresql(df, user, password):
    # Connecting to the database
    engine = create_db_engine(user, password)

    # Exporting the table to the database
    with engine.connect() as con:
        df.to_sql('weather_table', con=con, if_exists='replace', 
              index=False)

def create_flask_app(user, password):
    # Creating the flask server instance
    app = Flask(__name__)

    # Defining the single route for the frost events query
    @app.route('/frost_events', methods=['GET'])
    def get_frost_events():
        date_start = request.args.get('date_start', type=int)
        date_end = request.args.get('date_end', type=int)

        # Handling of invalid requests.
        # These are the cases I came up with, but with more testing, others may arise
        if date_start is None or date_end is None:
            return 'Bad request - invalid type or missing arguments!', 400
        if date_start < 0 or date_end < 0:
            return 'Bad request - invalid timestamp!', 400
        if date_start > date_end:
            return 'Bad request - date start after end!', 400

        # Connecting to the database
        engine = create_db_engine(user, password)

        # Getting the relevant data
        with engine.connect() as con:
            qu = text(f"""
                SELECT 
                    COUNT(*) FILTER (WHERE temperature <= 0) AS frost_events,
                    COUNT(*) FILTER (WHERE temperature <= -2) AS hard_frost_events
                FROM weather_table
                WHERE dt > :date_start AND dt < :date_end
                """)
            res = con.execute(qu, {'date_start': date_start, 'date_end': date_end}).first()

        # Returning the reply
        return jsonify({'frost_events': res[0], 'hard_frost_events': res[1]})

    return app
    
def main():
    weather_df = load_and_standardize('../data/owd.csv')
    load_to_postgresql(weather_df, user, password)
    app = create_flask_app(user, password)
    
    app.run()

if __name__ == "__main__":
    main()
