### Task 1

Out of all the tasks, this is the one, I've had the least previous experience with. So if anything is unclear, I hope this explanation answers our questions.

For setting up a database, I chose PostgreSQL. I have worked with it before, and its integration with python code is seamless. There's also a UI for accessing the databases but it's not needed for the execution of code, after Postgre is set up. I am using PostgreSQL 16. The distribution can be downloaded from this page: https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

I set up a database through the pgAdmin UI. I know that the task states that the more is done through scripting the better, but as stated before, I have little experience with databases, so this was a more fool-proof choice. After setting up the database with credentials, everything else (e.g. creating a table, uploading the data) was done through Python code.

For working with table data, I used Python 3.9 and Pandas. The server is set up with Flask library. One route was implemented, with the functionality described in the task. I accessed the server directly through a browser, an example query would be: http://localhost:5000/frost_events?date_start=1654000000&date_end=1655000000, yealding a result: {"frost_events":3,"hard_frost_events":1}

I was somewhat confused by the task because every row in the csv file contains data on multiple events, grouped by dates. To assure an easier working process with the data, I preprocessed it, the motivations for my choices are further explained in the data_study.ipynb notebook.
